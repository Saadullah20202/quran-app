import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
declare var Connection: any;
/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectivityProvider {
  onDevice: boolean;
  constructor( public network:Network) {
    console.log('Hello ConnectivityProvider Provider');
  }

  isOnline(): boolean {
    if(this.onDevice && this.network.type){
      alert(Connection.NONE);
      return Network !== Connection.NONE;
    } else {
      return navigator.onLine; 
    }
  }

  isOffline(): boolean {
    if(this.onDevice && this.network.type){
      return this.network.type === Connection.NONE;
    } else {
      return !navigator.onLine;   
    }
  }
}
