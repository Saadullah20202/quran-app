import { Component, ViewChild } from '@angular/core';
import { ToastController, NavController, NavParams, Navbar, Platform } from 'ionic-angular';
import { Media, MediaObject } from '@ionic-native/media';
import { BackgroundMode } from '@ionic-native/background-mode';
import { ContactPage } from '../contact/contact';
import { AboutPage } from '../about/about';
import { _ParseAST } from '@angular/compiler';

/**
 * Generated class for the MediaplayerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
const PLAYER_INTERVAL = 0.001;

@Component({
  selector: 'page-mediaplayer',
  templateUrl: 'mediaplayer.html',
})
export class MediaplayerPage {

  @ViewChild(Navbar) navBar: Navbar;
  flag: boolean = false;
  flagcount: number = 0;

  check: boolean = false;
  url: any;
  name: any;
  popup_menu: any;
  duration: any = -1;
  /* New Code4 */
  curr_playing_file: MediaObject;
  get_duration_interval: any;
  get_position_interval: any;
  position: any = 0;
  message: any;
  is_playing: boolean = false;
  is_in_play: boolean = false;
  is_ready: boolean = false;
  BGisEnabled:boolean=false;


  constructor(public navCtrl: NavController,
    private toastCtrl: ToastController,
    public navParams: NavParams,
    public media: Media,
    public backgroundMode: BackgroundMode,
    public platform: Platform) {
    this.url = navParams.data.url;
    this.name = navParams.data.name;
    this.popup_menu = true;
    this.backgroundMode.enable();
    platform.ready().then(() => {
      this.getDurationAndSetToPlay();
    }).catch(err => {
      console.log(err);
    });

    this.backgroundMode.on("activate").subscribe((a)=>{
            this.BGisEnabled=true;
      });
      platform.resume.subscribe(async () => {
        this.BGisEnabled=false;
      });

  }




  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionPage');
    this.url = this.navParams.data.url;
    this.navBar.backButtonClick = (e: UIEvent) => {
      this.navCtrl.pop();
      this.curr_playing_file.stop();
      this.check = false;
    }
  }
  ionViewWillEnter() {

  }
  ionViewWillLeave() {
    this.curr_playing_file.stop();
    this.check = false;

  }

  popover() {
    if (this.popup_menu) {
      this.popup_menu = false;
    } else {
      this.popup_menu = true;
    }
  }

  contact() {
    this.popup_menu = true;
    this.navCtrl.push(ContactPage);
  }

  about() {
    this.popup_menu = true;
    this.navCtrl.push(AboutPage);
  }

  createAudioFile() {
    return this.media.create(this.url);
  }

  getDurationAndSetToPlay() {
    let choice: Boolean = null;
    this.curr_playing_file = this.createAudioFile();
    this.backgroundMode.enable();

    this.backgroundMode.on('activate').subscribe((data) => {

      this.getAndSetCurrentAudioPosition()
    });
    this.curr_playing_file.play();
    this.curr_playing_file.setVolume(0.0);
    let self = this;
    self.get_duration_interval = setInterval(function () {

      if (self.duration == -1) {
        self.duration = ~~(self.curr_playing_file.getDuration());  // Double tilde converts some types to int

      } else {

        self.curr_playing_file.stop();
        this.check = false;
        self.curr_playing_file.release();
        self.setRecordingToPlay();
        //Get Duration return value in seconds

        clearInterval(self.get_duration_interval);
      }
    }, 1000);

  }

  getAndSetCurrentAudioPosition() {

    let diff = 1;
    // let check=true;
    let self = this;
    this.get_position_interval = setInterval(function () {

      let last_position = self.position;
      self.curr_playing_file.getCurrentPosition().then((position) => {
        if(self.BGisEnabled==false)
        {
          if (position >= 0 && position < self.duration) {
            if (Math.abs(last_position - position) >= diff) {
              // set position
              self.curr_playing_file.seekTo(last_position * 1000);//seekto expects value in milliSeconds
            } else {
              // update position for display
              self.position = position;
            }
          } else if (position >= self.duration) {
            self.stopPlayRecording();
            self.setRecordingToPlay();
          }
        }
        else{
          if (position >= 0 && position < self.duration) {
            self.position = position;
          }
          else if (position >= self.duration) {
            self.stopPlayRecording();
            self.setRecordingToPlay();
          }

        }

      });
    }, 100);
  }

  setRecordingToPlay() {
    this.curr_playing_file = this.createAudioFile();
    this.curr_playing_file.onStatusUpdate.subscribe(status => {
      // 2: playing
      // 3: pause
      // 4: stop
      this.message = status;
      switch (status) {
        case 1:
          this.is_in_play = false;
          break;
        case 2:   // 2: playing
          this.is_in_play = true;
          this.is_playing = true;
          break;
        case 3:   // 3: pause
          this.is_in_play = true;
          this.is_playing = false;
          break;
        case 4:   // 4: stop
        default:
          this.is_in_play = false;
          this.is_playing = false;
          break;
      }
    })
    console.log("audio file set");
    this.message = "audio file set";
    this.is_ready = true;
    this.getAndSetCurrentAudioPosition();
  }

  playRecording() {
    this.curr_playing_file.play();
    this.check = true;
  }

  pausePlayRecording() {
    this.curr_playing_file.pause();

  }


  stopPlayRecording() {
    this.check = false;
    this.curr_playing_file.stop();
    this.curr_playing_file.release();
    clearInterval(this.get_position_interval);
    this.position = 0;
  }

  change(pos) {//listener when you change the range position by cursor/pointer
    if (pos <= 0 && this.check == true) {
      this.stop();
    }

  }
  controlSeconds(action) {
    let step = 15;

    let number = this.position;
    switch (action) {
      case 'back':
        this.position = number < step ? PLAYER_INTERVAL : number - step;//PLAYER_INTERVAL=0.001
        break;
      case 'forward':
        this.position = number + step < this.duration ? number + step : this.duration;
        break;
      default:
        break;
    }
  }

  stop() {
    this.check = false;
    this.curr_playing_file.stop();
    this.curr_playing_file.release();
    // clearInterval(this.get_position_interval);
    this.position = 0;
  }


  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 9000,
      position: 'buttom'
    });
    toast.present();
  }
}
