import { Component } from '@angular/core';
import { NavController, Events, ToastController } from 'ionic-angular';
import { VolcategoryPage } from '../volcategory/volcategory';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { MediaplayerPage } from '../mediaplayer/mediaplayer';
import { StreamingMedia } from '@ionic-native/streaming-media';
import { ContactPage } from '../contact/contact';
import { AboutPage } from '../about/about';
import * as $ from 'jquery';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  popup_menu:any;
  myloader:boolean = false;
  isDisableButton:boolean = false;
  volume:any = [{
    id:1,
    volume: "Volume 1"
  },
  {
    id:2,
    volume: "Volume 2"
  },
  {
    id:3,
    volume: "Volume 3"
  },
  {
    id:4,
    volume: "Volume 4"
  },
  {
    id:5,
    volume: "Volume 5"
  },
  {
    id:6,
    volume: "Volume 6"
  }];
  name:any;
  liveurl:any;
  liveimg:any;
  last_seen:any;
  volume_id:any;
  surah_name:any;
  subheader_online:any;
  subheader_offline:any;
  imgurl:any = 'http://lecfeed.uk/quran/img1.jpg';

  constructor(public navCtrl: NavController,public navParams:NavParams, public event: Events,
              public network:ConnectivityProvider,
              private streamingMedia: StreamingMedia,
              public toastCtrl:ToastController
            ) {

        console.log("Before event info");

      event.subscribe('last_tafheem_seen', ()=> {
        this.last_seen = JSON.parse(localStorage.getItem('last_tafheem_seen'));
        if(this.last_seen != null){
          if(this.last_seen.online == true){
              this.liveurl = this.last_seen.liveurl;
              this.liveimg = this.last_seen.liveimg;
              this.volume_id = this.last_seen.volume_id;
              this.surah_name = this.last_seen.surahname;
              this.subheader_online = false;
              this.subheader_offline = true;
          }else{
            this.liveurl = this.last_seen.liveurl;
            this.volume_id = this.last_seen.volume_id;
            this.surah_name = this.last_seen.surahname;
            this.subheader_offline = false;
            this.subheader_online = true;
          }
        }
      });
      this.popup_menu = true;
      this.name = navParams.data.name;
      this.subheader_online = true;
      this.subheader_offline = true;
      this.last_seen = JSON.parse(localStorage.getItem('last_tafheem_seen'));
      if(this.last_seen != null){
        if(this.last_seen.online == true){
            this.liveurl = this.last_seen.liveurl;
            this.liveimg = this.last_seen.liveimg;
            this.volume_id = this.last_seen.volume_id;
            this.surah_name = this.last_seen.surahname;
            this.subheader_online = false;
            this.subheader_offline = true;
        }else{
            this.liveurl = this.last_seen.liveurl;
            this.volume_id = this.last_seen.volume_id;
            this.surah_name = this.last_seen.surahname;
            this.subheader_offline = false;
            this.subheader_online = true;
        }
      }
  }

  category(id,name){
    this.navCtrl.push(VolcategoryPage,{
      id:id,
      name:name
    });
  }

  playonline(liveurl,liveimg){
    this.presentToast('This functionality is disable now');
    // if(this.network.isOnline()){
    //   let options: StreamingAudioOptions = {
    //     bgImage : liveurl,                            //this.url+'img1.jpg',
    //     successCallback: () => { console.log('Video played') },
    //     errorCallback: (e) => { console.log('Error streaming') }
    //   }
    //   this.streamingMedia.playAudio(liveurl, options);
    // }else{
    //   this.presentToast('Interner Not Avaible');
    // }
  }

  playoffline(liveurl,surah_name){

  this.navCtrl.push(MediaplayerPage,{
    url: liveurl,
    name: surah_name
  });
  }
  popover(){
    if(this.popup_menu){
      this.popup_menu = false;
    }else{
      this.popup_menu = true;
    }
  }

  contact(){
    this.popup_menu = true;
    this.navCtrl.push(ContactPage);
  }

  about(){
    this.popup_menu = true;
    this.navCtrl.push(AboutPage);
  }


  /* ---------------------------- Toast Function ---------------------------------------*/
private presentToast(text) {
  let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: 'buttom'
   });
toast.present();
}
/* ----------------------------END Toast Function ---------------------------------------*/


download(i){
  $('#download_spinner_'+i).show();
  $("#button_"+i).attr("disabled", true);
}

}
