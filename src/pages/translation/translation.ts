import { Component, NgZone } from '@angular/core';
import {  NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { StreamingMedia, StreamingAudioOptions } from '@ionic-native/streaming-media';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import * as $ from 'jquery';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { MediaplayerPage } from '../mediaplayer/mediaplayer';
import { ContactPage } from '../contact/contact';
import { AboutPage } from '../about/about';

/**
 * Generated class for the TranslationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-translation',
  templateUrl: 'translation.html',
})
export class TranslationPage {

  cat_id:any;
  cat_name:any;
  counter:number = 0;
  translationdata:any;
  surah_data:any;
  surahdata:any;
  url:any;
  options:any;
  loader:any;
  lastseen:any;
  lastseendata:any;
  surahname:any;
  liveurl:any;
  imgurl:any ;
  subheader_online_urdu:any;
  subheader_online_pushto:any;
  subheader_online_hindi:any;
  subheader_offline_urdu:any;
  subheader_offline_pushto:any;
  subheader_offline_hindi:any;
  popup_menu:any;
  virtual_array:any;
  searchTerm : any="";
  downloadStatus:any;



  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http:Http,
              private sqlite:SQLite,
              private toastCtrl:ToastController,
              public transfer:FileTransfer,
              private file: File,
              public network:ConnectivityProvider,
              public event : Events,
              public zone : NgZone,
              private streamingMedia: StreamingMedia) {

                this.virtual_array = [];
      event.subscribe('last_seen', () => {
        this.updatelasetseen();
      });

            this.subheader_offline_hindi = true;
            this.subheader_offline_pushto = true;
            this.subheader_offline_urdu = true;
            this.subheader_online_hindi = true;
            this.subheader_online_pushto = true;
            this.subheader_online_urdu = true;
            this.popup_menu = true;

            this.options = {
              name:'quran.db',
              location: 'default'
            }
            this.loader = false;
            this.url ='http://lecfeed.uk/quran/translation/';
            this.imgurl = 'http://lecfeed.uk/quran/img1.jpg';
            this.cat_id = navParams.data.cat_id;
            this.cat_name = navParams.data.cat_name;


            if(this.cat_id == 1){
              if(localStorage.getItem('last_urdu_seen') != null){
                   this.lastseendata =  JSON.parse(localStorage.getItem('last_urdu_seen'));

                   this.liveurl = this.lastseendata.liveurl;
                   this.surahname = this.lastseendata.surahname;
                   this.imgurl = this.lastseendata.imgurl;

                   if(this.lastseendata.online == true){
                     this.subheader_online_urdu = false;
                     this.subheader_offline_hindi = true;
                     this.subheader_offline_pushto = true;
                     this.subheader_offline_urdu = true;
                     this.subheader_online_hindi = true;
                     this.subheader_online_pushto = true;
                   }else{
                     this.subheader_offline_urdu = false;
                     this.subheader_offline_hindi = true;
                     this.subheader_offline_pushto = true;
                     this.subheader_online_hindi = true;
                     this.subheader_online_pushto = true;
                     this.subheader_online_urdu = true;

                   }
              }
            }else if(this.cat_id == 2){
              if(localStorage.getItem('last_pushto_seen') != null){
                this.lastseendata =  JSON.parse(localStorage.getItem('last_pushto_seen'));
                this.liveurl = this.lastseendata.liveurl;
                this.surahname = this.lastseendata.surahname;
                this.imgurl = this.lastseendata.imgurl;
                if(this.lastseendata.online == true){
                  this.subheader_online_urdu = true;
                  this.subheader_offline_hindi = true;
                  this.subheader_offline_pushto = true;
                  this.subheader_offline_urdu = true;
                  this.subheader_online_hindi = true;
                  this.subheader_online_pushto = false;
                }else{
                  this.subheader_offline_urdu = true;
                  this.subheader_offline_hindi = true;
                  this.subheader_offline_pushto = false;
                  this.subheader_online_hindi = true;
                  this.subheader_online_pushto = true;
                  this.subheader_online_urdu = true;

                }
                  }
            }else if(this.cat_id == 3){
              if(localStorage.getItem('last_hindi_seen') != null){
                this.lastseendata =  JSON.parse(localStorage.getItem('last_hindi_seen'));
                this.liveurl = this.lastseendata.liveurl;
                this.surahname = this.lastseendata.surahname;
                this.imgurl = this.lastseendata.imgurl;

               if(this.lastseendata.online == true){
                  this.subheader_online_urdu = true;
                  this.subheader_offline_hindi = true;
                  this.subheader_offline_pushto = true;
                  this.subheader_offline_urdu = true;
                  this.subheader_online_hindi = false;
                  this.subheader_online_pushto = true;
                }else{
                  this.subheader_offline_urdu = true;
                  this.subheader_offline_hindi = false;
                  this.subheader_offline_pushto = true;
                  this.subheader_online_hindi = true;
                  this.subheader_online_pushto = true;
                  this.subheader_online_urdu = true;

                }
                  }
            }


              this.http.get('assets/json/urdu_surah_data.json')
              .subscribe(res => {
                this.translationdata = res.json().data;
                this.translationdata.map((e)=>{
                  this.counter = 0;
                  e.surah_data.map((ee) => {
                    this.counter = this.counter + 1;
                    ee.live_path = this.url+e.cat_id + '/' + this.counter+ '.mp3';
                  });
               });
               this.gettranslationdata();
              },err => {
                  console.log('err is '+err);
              });


  }

  gettranslationdata(){
    for(let data of this.translationdata){
      if(parseInt(data.cat_id) == parseInt(this.cat_id)){
        this.surah_data = data.surah_data;
        this.insertdata(this.surah_data,this.cat_id);
      }
    }
  }

  insertdata(array,cat_id){

    var db_data;
    this.sqlite.create(this.options)
    .then((db:SQLiteObject) => {
      for(let i=0;i<array.length; i++){
        console.log(array[i].surah_id);

         ///////////////////////////////////////Inserting data into SQLITE Table ///////////////////////////////
      db.executeSql('INSERT INTO translation(surah_id,cat_id,surah_name,live_path,download_path,download_flag) VALUES(?,?,?,?,?,?)',[array[i].surah_id,cat_id,array[i].surah_name,array[i].live_path,array[i].download_path,array[i].download_flag])
      .then((data) => {

      }).catch(inserterr => {

      });
      }
      this.refreshdata(this.cat_id);
    }).catch(error => {
      this.presentToast('Something wents wrong');
    });
  }


  refreshdata(cat_id){
      this.surahdata = [];
      this.sqlite.create(this.options)
      .then((db:SQLiteObject) => {
         ////////////////////////////////////////// get Data From Database and Parse into Array ////////////////////////////////////
         db.executeSql('SELECT * FROM translation WHERE cat_id=?',[cat_id])
         .then((data) => {
           for(let j=0 ; j<data.rows.length; j++){
             this.surahdata.push({
               surah_id: data.rows.item(j).surah_id,
               surah_name: data.rows.item(j).surah_name,
               live_path : data.rows.item(j).live_path,
               download_path : data.rows.item(j).download_path,
               download_flag : data.rows.item(j).download_flag
             });
           }
            this.loader = true;
            this.virtual_array = this.surahdata;
            // alert(JSON.stringify(this.surahdata));
         }).catch((error) => {

             this.presentToast('No Data Found');
         });
      }).catch(error => {
              this.presentToast('Something wents wrong');
      });

  }

      download(index,surah_id,cat_id,url, name){

        if(this.network.isOnline()){
          this.presentToast('Download Started');
          var savefile;
          $('#download_btn_'+index).attr("disabled", true);
          $('#download_spinner_'+index).show();
          $('#download_status_'+index).show();

          const fileTransfer: FileTransferObject = this.transfer.create();

          fileTransfer.onProgress((progressEvent) => {
            // alert(progressEvent);
            this.zone.run(() =>
                 this.downloadStatus=(progressEvent.lengthComputable)? Math.floor(progressEvent.loaded/progressEvent.total *100): -1 );
          });
          fileTransfer.download(url, this.file.dataDirectory + '/quran/'+this.cat_name+'/'+cat_id+'/'+name+'/'+surah_id+'.mp3').then((entry) => {
            savefile = entry.toURL();
            $('#download_spinner_'+index).hide();
            $('#download_status_'+index).hide();
            this.updatedata(surah_id,cat_id, savefile,index);
          });
        }else{
          this.presentToast('Interner Not Avaible');
        }

      }


      updatedata(surah_id, cat_id, download_path,index){
        this.presentToast('Sucessfully Download');
        var download_flag_value = 'yes'
          this.sqlite.create(this.options)
          .then((db:SQLiteObject) => {
              db.executeSql('UPDATE translation SET download_path=? ,download_flag=? WHERE surah_id=?',[download_path,download_flag_value,surah_id])
              .then((data) => {

              this.surahdata[index].download_path = download_path;
              this.surahdata[index].download_flag = download_flag_value;

              }).catch(error => {

              })
          }).catch(error => {
            console.log('err => ',error);
          });
      }



  ionViewDidLoad() {

  }


playdata(cat_id,liveurl, name){

    this.lastseen = {
      cat_id:cat_id,
      surahname:name,
      liveurl:liveurl,
      imgurl:null,
      online:false
    }
    if(cat_id == 1){
      localStorage.setItem('last_urdu_seen',JSON.stringify(this.lastseen));
    }else if(cat_id == 2){
      localStorage.setItem('last_pushto_seen',JSON.stringify(this.lastseen));
    }else if(cat_id == 3){
      localStorage.setItem('last_hindi_seen',JSON.stringify(this.lastseen));
    }
    this.event.publish('last_seen');

    let options: StreamingAudioOptions = {

      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') }
    }


    this.navCtrl.push(MediaplayerPage,{
      url: liveurl,
      name: name
    });
  }

  onlineplaydata(cat_id,surahname,liveurl, imgurl){

        this.lastseen = {
          surahname:surahname,
          liveurl:liveurl,
          imgurl:imgurl,
          online:true
        };

        if(cat_id == 1){
          localStorage.setItem('last_urdu_seen',JSON.stringify(this.lastseen));
        }else if(cat_id == 2){
          localStorage.setItem('last_pushto_seen',JSON.stringify(this.lastseen));
        }else if(cat_id == 3){
          localStorage.setItem('last_hindi_seen',JSON.stringify(this.lastseen));
        }

        this.event.publish('last_seen');

        if(this.network.isOnline()){
          let options: StreamingAudioOptions = {
            bgImage : imgurl,                            //this.url+'img1.jpg',
            successCallback: () => { console.log('Video played') },
            errorCallback: (e) => { console.log('Error streaming') }
          }
          this.streamingMedia.playAudio(liveurl, options);
        }else{
          this.presentToast('Interner Not Avaible');
        }

    }

  /* ---------------------------- Toast Function ---------------------------------------*/
private presentToast(text) {
  let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: 'buttom'
   });
toast.present();
}
/* ----------------------------END Toast Function ---------------------------------------*/

playonline(liveurl,liveimg){
  this.presentToast('This functionality is disable now');
  // if(this.network.isOnline()){
  //   let options: StreamingAudioOptions = {
  //     bgImage : liveurl,                            //this.url+'img1.jpg',
  //     successCallback: () => { console.log('Video played') },
  //     errorCallback: (e) => { console.log('Error streaming') }
  //   }
  //   this.streamingMedia.playAudio(liveurl, options);
  // }else{
  //   this.presentToast('Interner Not Avaible');
  // }
}

playoffline(liveurl,surah_name){

this.navCtrl.push(MediaplayerPage,{
  url: liveurl,
  name: surah_name
});
}

updatelasetseen(){

  if(this.cat_id == 1){
    if(localStorage.getItem('last_urdu_seen') != null){
         this.lastseendata =  JSON.parse(localStorage.getItem('last_urdu_seen'));

         this.liveurl = this.lastseendata.liveurl;
         this.surahname = this.lastseendata.surahname;


         if(this.lastseendata.online == true){
           this.subheader_online_urdu = false;
           this.subheader_offline_hindi = true;
           this.subheader_offline_pushto = true;
           this.subheader_offline_urdu = true;
           this.subheader_online_hindi = true;
           this.subheader_online_pushto = true;
         }else{
           this.subheader_offline_urdu = false;
           this.subheader_offline_hindi = true;
           this.subheader_offline_pushto = true;
           this.subheader_online_hindi = true;
           this.subheader_online_pushto = true;
           this.subheader_online_urdu = true;

         }
    }
  }else if(this.cat_id == 2){
    if(localStorage.getItem('last_pushto_seen') != null){
      this.lastseendata =  JSON.parse(localStorage.getItem('last_pushto_seen'));
      this.liveurl = this.lastseendata.liveurl;
      this.surahname = this.lastseendata.surahname;
      if(this.lastseendata.online == true){
        this.subheader_online_urdu = true;
        this.subheader_offline_hindi = true;
        this.subheader_offline_pushto = true;
        this.subheader_offline_urdu = true;
        this.subheader_online_hindi = true;
        this.subheader_online_pushto = false;
      }else{
        this.subheader_offline_urdu = true;
        this.subheader_offline_hindi = true;
        this.subheader_offline_pushto = false;
        this.subheader_online_hindi = true;
        this.subheader_online_pushto = true;
        this.subheader_online_urdu = true;

      }
        }
  }else if(this.cat_id == 3){
    if(localStorage.getItem('last_hindi_seen') != null){
      this.lastseendata =  JSON.parse(localStorage.getItem('last_hindi_seen'));
      this.liveurl = this.lastseendata.liveurl;
      this.surahname = this.lastseendata.surahname;

     if(this.lastseendata.online == true){
        this.subheader_online_urdu = true;
        this.subheader_offline_hindi = true;
        this.subheader_offline_pushto = true;
        this.subheader_offline_urdu = true;
        this.subheader_online_hindi = false;
        this.subheader_online_pushto = true;
      }else{
        this.subheader_offline_urdu = true;
        this.subheader_offline_hindi = false;
        this.subheader_offline_pushto = true;
        this.subheader_online_hindi = true;
        this.subheader_online_pushto = true;
        this.subheader_online_urdu = true;

      }
        }
  }

}

popover(){
  if(this.popup_menu){
    this.popup_menu = false;
  }else{
    this.popup_menu = true;
  }
}

contact(){
  this.popup_menu = true;
  this.navCtrl.push(ContactPage);
}

about(){
  this.popup_menu = true;
  this.navCtrl.push(AboutPage);
}


getItems(ev: any) {
  // Reset items back to all of the items
  this.initializeItems();

  // set val to the value of the searchbar
  const val = ev.target.value;
  // if the value is an empty string don't filter the items
  if (val && val.trim() != '') {
      this.surahdata = this.surahdata.filter((item) => {
       return (item.surah_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
  });
  }
}

  initializeItems(){
    this.surahdata = this.virtual_array;
    }

  //   setFilteredItems() {

  //     this.surahdata = this.surahdata.filterItems(this.searchTerm);

  // }


}
