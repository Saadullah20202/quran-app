import { Component } from '@angular/core';
import {  NavController, NavParams,ToastController } from 'ionic-angular';
import { StreamingMedia, StreamingAudioOptions } from '@ionic-native/streaming-media';

/**
 * Generated class for the RakooPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-rakoo',
  templateUrl: 'rakoo.html',
})
export class RakooPage {
    surah_name:any;
    length:any;
    surah_id:any;
    vol_id:any;
    rukoo:any = [];
    url:any = 'http://lecfeed.uk/quran/';
    debacha:any;
    muqadma:any;
    rakoo_hidden:any;
    debacha_hidden:any;
    muqadma_hidden:any;
    rakoo_data:any;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private streamingMedia: StreamingMedia,
              private toastCtrl:ToastController) {

    this.surah_id = navParams.data.surah_id;
    this.surah_name = navParams.data.surah_name;
    this.vol_id = navParams.data.vol_id;
    this.length = navParams.data.length;
    this.debacha_hidden = true;
    this.rakoo_hidden = true;
    this.muqadma_hidden = true;
    this.rakoo_data = [];
    this.debacha = [];
    this.muqadma = [];

    if(this.surah_name == 'Deebacha'){
      this.debacha_hidden = false;
      this.debacha = [
        {
          id:1,
          vol_id: this.vol_id,
          surah_id : this.surah_id,
          rakoo_name:this.surah_name,
          live_url: this.url+this.vol_id+'/'+this.surah_id+'/'+1+'.mp3',
          download_url: null,
          download_flag: null
        }
      ];
    }else if(this.surah_name == 'Muqadma'){
      this.muqadma_hidden = false;
      this.muqadma = [
        {
          id:1,
          vol_id: this.vol_id,
          surah_id : this.surah_id,
          rakoo_name:this.surah_name,
          live_url: this.url+this.vol_id+'/'+this.surah_id+'/'+1+'.mp3',
          download_url: null,
          download_flag: null
        }
      ];
    }else{
      this.rakoo_hidden = false;
      for(let i=1;i<=this.length;i++){
        if(i == 1){
            this.rakoo_data.push(
              {
              id:i,
               vol_id: this.vol_id,
               surah_id : this.surah_id,
               rakoo_name: 'Intro ',
               live_url: this.url+this.vol_id+'/'+this.surah_id+'/'+i+'.mp3',
               download_url: null,
               download_flag: null
              },

            );
        }else{
          this.rakoo_data.push(
            {id:i,
             vol_id: this.vol_id,
             surah_id : this.surah_id,
             rakoo_name: 'Rukoo ' + (i-1),
             live_url: this.url+this.vol_id+'/'+this.surah_id+'/'+i+'.mp3',
             download_url: null,
             download_flag: null
            }
          );
        }
      }
    }



  }

  playdata(url){
      let options: StreamingAudioOptions = {
        bgImage : this.url+'img1.jpg',
        successCallback: () => { console.log('Video played') },
        errorCallback: (e) => { console.log('Error streaming') }
      }
      this.streamingMedia.playAudio(url, options);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RakooPage');
  }

   /* ---------------------------- Toast Function ---------------------------------------*/
   private presentToast(text) {
    let toast = this.toastCtrl.create({
          message: text,
          duration: 3000,
          position: 'buttom'
     });
toast.present();
}
/* ----------------------------END Toast Function ---------------------------------------*/
}
