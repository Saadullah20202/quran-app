import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContactPage } from '../contact/contact';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  popup_menu:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.popup_menu = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  popover(){
    if(this.popup_menu){
      this.popup_menu = false;
    }else{
      this.popup_menu = true;
    }
  }
  
  contact(){
    this.popup_menu = true;
    this.navCtrl.push(ContactPage);
  }

  about(){
    this.popup_menu = true;
    this.navCtrl.push(AboutPage);
  }



}
