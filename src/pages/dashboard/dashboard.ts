import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { TranslationPage } from '../translation/translation';
import { ContactPage } from '../contact/contact';
import { AboutPage } from '../about/about';


/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  translation_cat:any;
  translate_data:any;
  popup_menu:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
             public http:Http) {
          this.translate_data = true;
          this.popup_menu = true;
          http.get('assets/json/translation_cat.json')
          .map(res => res.json())
          .subscribe(data => {
            this.translation_cat = data.data;
          },err =>{
            console.log('error is '+ err);
          });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  tafheemulquran(name){

    this.navCtrl.push(HomePage,{
      name:name
    });
  }

  translation(){

    this.translate_data = false;
  }

  category(id, cat_name){

        this.navCtrl.push(TranslationPage, {
            cat_id : id,
            cat_name : cat_name
        });
  }
  popover(){
    if(this.popup_menu){
      this.popup_menu = false;
    }else{
      this.popup_menu = true;
    }
  }

  contact(){
    this.popup_menu = true;
    this.navCtrl.push(ContactPage);
  }

  about(){
    this.popup_menu = true;
    this.navCtrl.push(AboutPage);
  }



}
