import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AboutPage } from '../about/about';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  popup_menu:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.popup_menu = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  popover(){
    if(this.popup_menu){
      this.popup_menu = false;
    }else{
      this.popup_menu = true;
    }
  }

  contact(){
    this.popup_menu = true;
    this.navCtrl.push(ContactPage);
  }

  about(){
    this.popup_menu = true;
    this.navCtrl.push(AboutPage);
  }



}
