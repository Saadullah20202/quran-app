import { Component, NgZone } from '@angular/core';
import {  NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { StreamingMedia, StreamingAudioOptions } from '@ionic-native/streaming-media';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import * as $ from 'jquery'
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { MediaplayerPage } from '../mediaplayer/mediaplayer';
import { ContactPage } from '../contact/contact';
import { AboutPage } from '../about/about';




/**
 * Generated class for the VolcategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-volcategory',
  templateUrl: 'volcategory.html',
})
export class VolcategoryPage {

  volume_id:any;
  surahdata:any;
  url:any ;
  vol_surah:any;
  download_btn:any;
  play_btn:any;
  options:any;
  virtual_array:any;
  imgurl:any = 'http://lecfeed.uk/quran/img1.jpg';
  loader:any;
  download_spinner:any;
  stop_btn:any;
  name:any;
  lasttafheemseen:any;
  popup_menu:any;



  constructor(public navCtrl: NavController, public navParams: NavParams,
              private streamingMedia: StreamingMedia,
              public http:Http,
              private sqlite:SQLite,
              private toastCtrl:ToastController,
              public transfer:FileTransfer,
              private file: File,
              public network:ConnectivityProvider,
              public event : Events,
              public zone: NgZone
             ) {
    this.download_btn = false;
    this.download_spinner = true;
    this.popup_menu = true;
    this.play_btn = false;
    this.volume_id = navParams.data.id;
    this.loader = false;
    this.stop_btn = true;
    this.virtual_array = [];
    this.surahdata = [];
    this.name = navParams.data.name;
    this.options = {
      name:'quran.db',
      location: 'default'
    }
    this.url ='http://lecfeed.uk/quran/';
    //this.url = 'http://192.168.60.134/quran/';
    http.get('assets/json/surah_data.json')
    .subscribe(data => {
        this.vol_surah = data.json().data;
        this.vol_surah.map((e) => {
          e.surah_data.map((ee) => {
            ee.live_path = this.url+ e.vol_id + '/' + ee.surah_id+ '.mp3';
          });
        });
        //console.log(this.vol_surah);
        this.getcatdata();
    },error => {
      console.log(error);
    });


  }

  // Get Data From Volume Array

  getcatdata(){
    var getdata;
    for(let volume_surah of this.vol_surah){
      if(this.volume_id == volume_surah.vol_id){

        this.inserteddata(volume_surah.surah_data,this.volume_id);

      }
    }
  }


// Insert and retrieve data from SQLITE DataBase
  inserteddata(array,volume_id){
      var db_data;
      this.sqlite.create(this.options)
      .then((db:SQLiteObject) => {

        for(let i=0;i<array.length; i++){
          console.log(array[i].surah_id);

       ///////////////////////////////////////Inserting data into SQLITE Table ///////////////////////////////
      db.executeSql('INSERT INTO quranfiles(surah_id,vol_id,surah_name,live_path,download_path,download_flag) VALUES(?,?,?,?,?,?)',[array[i].surah_id,volume_id,array[i].surah_name,array[i].live_path,array[i].download_path,array[i].download_flag])
      .then((data) => {
        console.log('Data is Inserted');
      }).catch(inserterr => {
       console.log(inserterr)
      });
    }
        this.refreshdata(volume_id);
      }).catch(error => {
        this.presentToast('Something wents wrong');
      });
      }

  //////////////////////////////////////////////Get Data Method ///////////////////////////////////////////////////////////
  refreshdata(vol_id){
    this.surahdata = [];
    this.sqlite.create(this.options)
    .then((db:SQLiteObject) => {
       ////////////////////////////////////////// get Data From Database and Parse into Array ////////////////////////////////////
       db.executeSql('SELECT * FROM quranfiles WHERE vol_id=?',[vol_id])
       .then((data) => {
         for(let j=0 ; j<data.rows.length; j++){
           this.surahdata.push({
             surah_id: data.rows.item(j).surah_id,
             surah_name: data.rows.item(j).surah_name,
             live_path : data.rows.item(j).live_path,
             download_path : data.rows.item(j).download_path,
             download_flag : data.rows.item(j).download_flag
           });
         }
         this.virtual_array = this.surahdata;
          this.loader = true;
          //alert(JSON.stringify(this.surahdata));
       }).catch((error) => {

           this.presentToast('No Data Found');
       });
    }).catch(error => {
            this.presentToast('Something wents wrong');
    });

  }



  download(index,surah_id,vol_id,url){

    if(this.network.isOnline()){
      this.presentToast('Downloading Started');
      var savefile;
      $('#download_btn_'+index).attr("disabled", true);
      $('#download_spinner_'+index).show();
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.download(url, this.file.dataDirectory + '/quran/'+vol_id+'/'+surah_id+'.mp3').then((entry) => {
        savefile = entry.toURL();
        $('#download_spinner_'+index).hide();
        this.updatedata(surah_id,vol_id, savefile,index);
      }).catch(err => {
        console.log('err is '+JSON.stringify(err));
      });
    }else{
      this.presentToast('Internet is Not Available');
    }

  }


  updatedata(surah_id, vol_id, download_path,index){
    this.presentToast('Successfully Downloaded');
    var download_flag_value = 'yes'
      this.sqlite.create(this.options)
      .then((db:SQLiteObject) => {
          db.executeSql('UPDATE quranfiles SET download_path=? ,download_flag=? WHERE surah_id=?',[download_path,download_flag_value,surah_id])
          .then((data) => {
            this.surahdata[index].download_path = download_path;
            this.surahdata[index].download_flag = download_flag_value;
          //  this.refreshdata(vol_id);
          }).catch(error => {
            console.log('not updated '+ JSON.stringify(error));
          })
      }).catch(error => {
        console.log(error);
      });
  }



  onlineplaydata(volume_id,surahname,liveurl, imgurl){

    this.lasttafheemseen = {
      tafheemname:this.name,
      volume_id:volume_id,
      surahname:surahname,
      liveurl:liveurl,
      imgurl:imgurl,
      online:true
    };

    localStorage.setItem('last_tafheem_seen',JSON.stringify(this.lasttafheemseen));
    this.event.publish('last_tafheem_seen');

    if(this.network.isOnline()){
      let options: StreamingAudioOptions = {
        bgImage : imgurl,                            //this.url+'img1.jpg',
        successCallback: () => { console.log('Video played') },
        errorCallback: (e) => { console.log('Error streaming') }
      }
      this.streamingMedia.playAudio(liveurl, options);
    }else{
      this.presentToast('Interner Not Avaible');
    }

}

playdata(volume_id,liveurl, name){

  this.lasttafheemseen = {
    tafheemname:this.name,
    volume_id:volume_id,
    surahname:name,
    liveurl:liveurl,
    imgurl:null,
    online:false
  }

  localStorage.setItem('last_tafheem_seen',JSON.stringify(this.lasttafheemseen));
  this.event.publish('last_tafheem_seen');

  // this.navCtrl.push(MediaplayerPage,{
  //   url: liveurl,
  //   name: name
  // });
  let options: StreamingAudioOptions = {
                           //this.url+'img1.jpg',
    successCallback: () => { console.log('Video played') },
    errorCallback: (e) => { console.log('Error streaming') }
  }
  //this.streamingMedia.playAudio(liveurl, options);
  this.navCtrl.push(MediaplayerPage,{
    url: liveurl,
    name: name
  });
}

popover(){
  if(this.popup_menu){
    this.popup_menu = false;
  }else{
    this.popup_menu = true;
  }
}

contact(){
  this.popup_menu = true;
  this.navCtrl.push(ContactPage);
}

about(){
  this.popup_menu = true;
  this.navCtrl.push(AboutPage);
}



stop(){

}

/* ---------------------------- Toast Function ---------------------------------------*/
private presentToast(text) {
  let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: 'buttom'
   });
toast.present();
}
/* ----------------------------END Toast Function ---------------------------------------*/

getItems(ev: any) {
  // Reset items back to all of the items
  this.initializeItems();

  // set val to the value of the searchbar
  const val = ev.target.value;
  // if the value is an empty string don't filter the items
  if (val && val.trim() != '') {
      this.surahdata = this.surahdata.filter((item) => {
       return (item.surah_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
  });
  }
}

  initializeItems(){
    this.surahdata = this.virtual_array;
    }

}
