import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = DashboardPage;

  constructor(platform: Platform, statusBar: StatusBar,   splashScreen: SplashScreen,public sqlite:SQLite) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      ///////////////////////////////////////// Create DataBase And Table Here ////////////////////////////////////////////

      sqlite.create({
        name: 'quran.db',
        location:'default'
      })
      .then((db:SQLiteObject)=>{
        db.executeSql('CREATE TABLE IF NOT EXISTS quranfiles(surah_id INTEGER PRIMARY KEY,vol_id INT, surah_name TEXT, live_path TEXT, download_path TEXT, download_flag TEXT)',{})
        .then((data) => {
           console.log('Table created');
        })
        .catch(error => {
          console.log('already exists')
        });
        db.executeSql('CREATE TABLE IF NOT EXISTS translation(surah_id INTEGER PRIMARY KEY,cat_id INT, surah_name TEXT, live_path TEXT, download_path TEXT, download_flag TEXT)',{})
        .then((data) => {
              console.log('Sucessfully Created ' +JSON.stringify(data));
        }).catch(error => {
              console.log(JSON.stringify(error));
        });  
      })
      .catch(error => {
        console.log('DB not Created');
      });
      /////////////////////////////////////////  Data Base and Table Created //////////////////////////////////////////////



      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

