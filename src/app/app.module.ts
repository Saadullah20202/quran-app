import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { VolcategoryPage } from '../pages/volcategory/volcategory';
import { RakooPage } from '../pages/rakoo/rakoo';
import { StreamingMedia } from '@ionic-native/streaming-media';
import {HttpModule} from '@angular/http';
import { SQLite } from '@ionic-native/sqlite';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { Network } from '@ionic-native/network';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Media } from '@ionic-native/media';
import { MediaplayerPage } from '../pages/mediaplayer/mediaplayer';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { TranslationPage } from '../pages/translation/translation';
import { ContactPage } from '../pages/contact/contact';
import { AboutPage } from '../pages/about/about';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    VolcategoryPage,
    RakooPage,
    MediaplayerPage,
    DashboardPage,
    TranslationPage,
    ContactPage,
    AboutPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    VolcategoryPage,
    RakooPage,
    MediaplayerPage,
    DashboardPage,
    TranslationPage,
    ContactPage,
    AboutPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    StreamingMedia,
    SQLite,
    File,
    Media,
    Network,
    FileTransfer,
    BackgroundMode,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConnectivityProvider,
  ]
})
export class AppModule {}
